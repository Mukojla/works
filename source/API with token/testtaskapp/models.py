from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True)
    number = models.IntegerField()

    @classmethod
    def like(cls, user, post):
        like = cls.objects.filter(user__id=user.id, post__id=post.id)

        if like.exists():
            like = like[0]
            if like.number < 1:
                like.number += 1
                like.save()
        else:
            cls.objects.create(number=1, user=user, post=post)

    @classmethod
    def dislike(cls, user, post):
        like = cls.objects.filter(user__id=user.id, post__id=post.id)

        if like.exists():
            like = like[0]
            if like.number < 1:
                like.number -= 1
                like.save()
        else:
            cls.objects.create(number=-1, user=user, post=post)

    def __str__(self):
        return '%s %s' % (self.user.username, self.number)
