from django.test import TestCase
from rest_framework.test import APIClient
from .models import User, Post, Like
from rest_framework.test import CoreAPIClient
from rest_framework.test import RequestsClient
import json
from django.urls import reverse


class APITestCase(TestCase):

    def setUp(self):
        self._api_client = APIClient()
        self._request_client = RequestsClient()

    def test_create_user(self):
        self._api_client.post(reverse('registration'), {'username': 'test01', 'password': 'test01'})
        self.assertEqual(User.objects.all().count(), 1)

    def test_get_token(self):
        User.objects.create_user(username='test02', password='test02')
        response = self._api_client.post('/api/token/', {'username': 'test02', 'password': 'test02'})
        self.assertEqual(response.status_code, 200)

    def _get_user_token(self, user, password):
        response = self._request_client.post('http://localhost/api/token/', {'username': user, 'password': password})
        try:
            return json.loads(response.text)['access']
        except KeyError:
            pass

    def test_create_post(self):
        User.objects.create_user(username='test03', password='test03')
        self._token = self._get_user_token('test03', 'test03')
        self._request_client.headers.update({'Authorization': 'Bearer {0}'.format(self._token)})
        response = self._request_client.post('http://localhost:8000/api/post/create', {'title': 'TestTitle', 'text': 'TestText'})

        self.assertEqual(response.status_code, 200)

    def test_put_like(self):
        User.objects.create_user(username='test01', password='test01')
        self._token = self._get_user_token('test01', 'test01')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.post('http://localhost:8000/api/post/create', {'title': 'TestTitle', 'text': 'TestText'})
        User.objects.create_user(username='test02', password='test02')
        self._token = self._get_user_token('test02', 'test02')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.put('http://localhost:8000/api/post/1/like')
        User.objects.create_user(username='test03', password='test03')
        self._token = self._get_user_token('test03', 'test03')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.put('http://localhost:8000/api/post/1/like')
        self.assertEqual(Post.objects.all()[0].like_set.filter(number=1).count(), 2)



    def test_put_dislike(self):
        User.objects.create_user(username='test01', password='test01')
        self._token = self._get_user_token('test01', 'test01')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.post('http://localhost:8000/api/post/create', {'title': 'TestTitle', 'text': 'TestText'})
        User.objects.create_user(username='test02', password='test02')
        self._token = self._get_user_token('test02', 'test02')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.put('http://localhost:8000/api/post/1/dislike')
        User.objects.create_user(username='test03', password='test03')
        self._token = self._get_user_token('test03', 'test03')
        self._request_client.headers.update({'Authorization': f'Bearer {self._token}'})
        self._request_client.put('http://localhost:8000/api/post/1/dislike')
        self.assertEqual(Post.objects.all()[0].like_set.filter(number=-1).count(), 2)
