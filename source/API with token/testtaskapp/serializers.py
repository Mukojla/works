from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Post


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

        fields = (
            'username', 'password'
        )


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'title', 'text'
        )

