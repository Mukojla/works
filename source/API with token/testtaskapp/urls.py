from testtaskapp import views
from django.urls import path


urlpatterns = [
    path('register', views.UserRegisterAPIView.as_view(), name='registration'),
    path('post/create', views.PostCreationAPIView.as_view(), name='postcreation'),
    path('post/<int:pk>/like', views.LikeAPIView.as_view(), name='like'),
    path('post/<int:pk>/dislike', views.DisLikeAPIView.as_view(), name='dislike')
    ]
