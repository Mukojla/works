from django.apps import AppConfig


class TesttaskappConfig(AppConfig):
    name = 'testtaskapp'
