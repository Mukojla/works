from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from .serializers import *
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from .models import Like


class UserRegisterAPIView(generics.CreateAPIView):
    serializer_class = UserRegisterSerializer

    def create(self, request):
        serializer = UserRegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        User.objects.create_user(username=data['username'], password=data['password'])

        resp = Response(
            data={
                'detail': '',
            },
            status=status.HTTP_200_OK
            )

        return resp


class PostCreationAPIView(generics.CreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        serializer = PostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        post = Post(**data)
        post.user = self.request.user
        post.save()
        resp = Response(
            data={
                'detail': '',
            },
            status=status.HTTP_200_OK
        )

        return resp


class LikeAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def update(self, request, *args, **kwargs):
        post = Post.objects.get(id=kwargs['pk'])
        Like.like(post=post, user=self.request.user)
        resp = Response(
            data={
                'detail': '',
            },
            status=status.HTTP_200_OK
        )

        return resp


class DisLikeAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def update(self, request, *args, **kwargs):
        post = Post.objects.get(id=kwargs['pk'])
        Like.dislike(post=post, user=self.request.user)
        resp = Response(
            data={
                'detail': '',
            },
            status=status.HTTP_200_OK
        )

        return resp
