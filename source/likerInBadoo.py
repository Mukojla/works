from selenium import webdriver
from time import sleep


login = ""  # enter your login here
password = ""  # enter your pass here
counter = 0

driver = webdriver.Chrome()
driver.get("https://badoo.com/signin/")
driver.maximize_window()
driver.implicitly_wait(2)


def firstlike():
    sleep(2)
    email = driver.find_element_by_xpath("//input[@name='email']")
    email.send_keys(login)
    passw = driver.find_element_by_xpath("//input[@name='password']")
    passw.send_keys(password)
    driver.find_element_by_xpath("//button[@name='post']").click()
    sleep(3)
    driver.find_element_by_xpath("//div[@data-choice='yes']").click()  # click like button
    sleep(3)
    driver.find_element_by_class_name('js-chrome-pushes-deny').click()  # miss notifications in browser


def infinitelike():
    global counter
    driver.find_element_by_xpath("//div[@data-choice='yes']").click()
    text_about_girl = driver.find_elements_by_xpath("//p[@class='profile-section__txt-line']")
    mutual_sympathy = driver.find_elements_by_class_name('js-ovl-close')
    if len(text_about_girl) > 0:
        sleep(1)
        driver.save_screenshot('screenie-{}.png'.format(counter))
        counter += 1
    if len(mutual_sympathy) > 0:
        mutual_sympathy[0].click()


firstlike()
while True:
    sleep(1)
    infinitelike()
