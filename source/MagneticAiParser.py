import io
import csv

from requests_html import HTMLSession
from flask import Flask, make_response, request

app = Flask(__name__)


@app.route("/TEST/test.html")
def print_out():
    gpparser = GPParser('https://play.google.com/store/apps/category/GAME')
    gpparser.parse_html()

    return make_response(gpparser.get_csv_data())


@app.route("/")
def search_results():
    search = request.args.get('search', '').strip('"')
    print(search)
    if not search:
        return ""

    gpparser = GPParser('https://play.google.com/store/apps/category/GAME')
    gpparser.parse_html()
    games = gpparser.get_games_app_names()
    print(type(games))
    return f"Game '{search}' successfully found" if search in games else f"Game '{search}' not found"


class GPParser:

    def __init__(self, url):
        self._url = url
        self._data = set()
        self._session = HTMLSession()

    def parse_html(self):
        response = self._session.get(self._url)
        if response.status_code != 200:
            return

        blocks = response.html.find('div.id-cluster-container.cluster-container.cards-transition-enabled')
        for block in blocks:
            category = block.find('a.title-link.id-track-click', first=True).text
            details = block.find('div.card.no-rationale.square-cover.apps.small')
            for detail in details:

                game = detail.find('span.preview-overlay-container', first=True).attrs['data-docid']

                self._data.add(f'APP/{category}/{game}')

    def get_games_app_category(self):
        categories = set()
        for el in self._data:
            category = el.split('/')[-2]
            categories.add(category)
        return categories

    def get_games_app_names(self):
        games = set()
        for el in self._data:
            game = el.split('/')[-1]
            games.add(game)
        return games

    def print(self):
        for el in self._data:
            print(el)

    def get_csv_data(self):
        output = io.StringIO()
        writer = csv.writer(output)
        writer.writerow(['Category', 'Name'])
        for el in self._data:
            _, category, game = el.split('/')
            writer.writerow([category, game])
        return output.getvalue()


if __name__ == '__main__':
    app.run(debug=True)
